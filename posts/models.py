from django.contrib.auth.models import User
from django.db import models


class Post(models.Model):
    description = models.TextField()

    def __str__(self):
        return self.description


class UserInfo(models.Model):
    user_id = models.OneToOneField(User, unique=True, on_delete=models.CASCADE)
    telegram = models.CharField(max_length=99)

    def __str__(self):
        return self.user_id.username + ' - ' + self.telegram


class Comments(models.Model):
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    post_id = models.ForeignKey(Post, on_delete=models.CASCADE)
    comment_description = models.TextField()

    def __str__(self):
        return self.user_id.username + ' - ' + self.comment_description
