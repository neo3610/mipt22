from django.contrib import admin

from posts.models import Post, UserInfo, Comments

admin.site.register(Post)
admin.site.register(UserInfo)
admin.site.register(Comments)