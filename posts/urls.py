from django.urls import path

from posts.views import ListPost, DetailPost, ListComment

urlpatterns = [
    path('posts/', ListPost.as_view()),
    path('posts/<int:pk>', DetailPost.as_view()),
    path('comments/', ListComment.as_view())
]