
from rest_framework import generics
from posts.models import Post, Comments
from posts.serializers import PostSerializer, CommentSerializer


class ListPost(generics.ListCreateAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializer


class DetailPost(generics.RetrieveUpdateDestroyAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializer


class ListComment(generics.ListCreateAPIView):
    queryset = Comments.objects.all()
    serializer_class = CommentSerializer